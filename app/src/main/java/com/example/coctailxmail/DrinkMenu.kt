package com.example.coctailxmail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Text
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.coctailxmail.databinding.FragmentDrinkMenuBinding
import com.example.coctailxmail.viewmodel.DrinkMenuViewModel
import com.example.coctailxmail.views.DrinkMenuAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DrinkMenu : Fragment() {

    private val drinkMenuVM by activityViewModels<DrinkMenuViewModel>()
    private var _binding: FragmentDrinkMenuBinding? = null
    private val drinkAdapter by lazy { DrinkMenuAdapter{
        category ->
            val bundle: Bundle = Bundle()
            bundle.putString("categoryFilter", category.strCategory)
            findNavController().navigate(R.id.drinkItemList, bundle)
    }

    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        //Not Recycler View, Fragment that contains the Recycler View
        return FragmentDrinkMenuBinding.inflate(inflater, container, false).apply {
            _binding = this
            //RV Fixtures is a xml element inside this fragment.
            rvFixtures.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = drinkAdapter
            }
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        drinkMenuVM.state.observe(viewLifecycleOwner) { state ->
            drinkAdapter.loadCategories(state.drinkCategories)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}