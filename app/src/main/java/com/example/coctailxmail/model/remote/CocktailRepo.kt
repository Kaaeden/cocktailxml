package com.example.coctailxmail.model.remote

import com.example.coctailxmail.model.dtos.CategoryResponse
import com.example.coctailxmail.model.entity.Category
import com.example.coctailxmail.model.entity.Drink
import com.example.coctailxmail.model.entity.DrinkItem
import com.example.coctailxmail.model.mapper.CategoryMapper
import com.example.coctailxmail.model.mapper.DrinkItemMapper
import com.example.coctailxmail.model.mapper.DrinkMapper
import javax.inject.Inject


class CocktailRepo @Inject constructor(private val service: CocktailService) {

    private val categoryMapper = CategoryMapper()
    private val drinkItemMapper = DrinkItemMapper()
    private val drinkMapper = DrinkMapper()
    suspend fun getCategories(): List<Category> {
        val repoResponse = service.getCocktailCategories("list")
        return repoResponse.drinks.map{ categoryMapper(it) }
    }

   suspend fun getDrinkListByCategory(category: String = "Cocktail"): List<DrinkItem> {
       val repoResponse = service.getDrinkListByCategory(category)
       println(repoResponse)
       return repoResponse.drinks.map{drinkItemMapper(it)}

   }

    suspend fun getDrinkDetailsByIdNum(id_: String): List<Drink> {
        val repoResponse = service.getDrinkDetailsById(id_)
        return repoResponse.drinks.map{drinkMapper(it)}
    }
}