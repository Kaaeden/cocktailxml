package com.example.coctailxmail.model.mapper

import com.example.coctailxmail.model.dtos.DrinkItemDTO
import com.example.coctailxmail.model.entity.DrinkItem


class DrinkItemMapper: Mapper<DrinkItemDTO, DrinkItem> {
    override fun invoke(dto: DrinkItemDTO): DrinkItem = with(dto) {
        DrinkItem(idDrink = idDrink,strDrink = strDrink, strDrinkThumb = strDrinkThumb)
    }
}