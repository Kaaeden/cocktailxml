package com.example.coctailxmail.model.dtos


@kotlinx.serialization.Serializable
data class DrinkItemResponse( val drinks: List<DrinkItemDTO> = emptyList())