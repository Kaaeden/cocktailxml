package com.example.coctailxmail.model.dtos

@kotlinx.serialization.Serializable
data class DrinkItemDTO(
    val idDrink: String?,
    val strDrink: String?,
    val strDrinkThumb: String?
)