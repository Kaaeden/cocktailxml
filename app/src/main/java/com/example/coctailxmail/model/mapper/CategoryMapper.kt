package com.example.coctailxmail.model.mapper

import com.example.coctailxmail.model.dtos.CategoryDTO
import com.example.coctailxmail.model.entity.Category

class CategoryMapper: Mapper<CategoryDTO, Category> {
    override fun invoke(dto: CategoryDTO): Category = with(dto) {
        Category(strCategory = strCategory)
    }
}