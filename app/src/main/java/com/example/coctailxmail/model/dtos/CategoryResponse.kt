package com.example.coctailxmail.model.dtos

import kotlinx.serialization.Serializable

@Serializable
data class CategoryResponse(val drinks: List<CategoryDTO> = emptyList() )