package com.example.coctailxmail.model.mapper

sealed interface Mapper<in DTO, out ENTITY> {
    operator fun invoke(dto: DTO) : ENTITY
}