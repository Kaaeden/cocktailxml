package com.example.coctailxmail.model.entity

@kotlinx.serialization.Serializable
data class DrinkItem(
    val idDrink: String? = "",
    val strDrink: String? = "",
    val strDrinkThumb: String? = ""
)