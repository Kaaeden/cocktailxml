package com.example.coctailxmail.model.dtos

import com.example.coctailxmail.model.entity.Category
import kotlinx.serialization.Serializable

@Serializable
data class CategoryDTO(val strCategory: String?)
