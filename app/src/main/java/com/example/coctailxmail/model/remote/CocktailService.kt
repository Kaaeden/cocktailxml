package com.example.coctailxmail.model.remote

import com.example.coctailxmail.model.dtos.CategoryResponse
import com.example.coctailxmail.model.dtos.DrinkItemResponse
import com.example.coctailxmail.model.dtos.DrinkResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface CocktailService {

    @GET("list.php")
    suspend fun getCocktailCategories(@Query("c")  category: String = "list"): CategoryResponse

    @GET("filter.php")
    suspend fun getDrinkListByCategory(@Query("c")  filterType: String = "list"): DrinkItemResponse

    @GET("lookup.php")
    suspend fun getDrinkDetailsById(@Query("i") drinkId: String = "11007"): DrinkResponse

}