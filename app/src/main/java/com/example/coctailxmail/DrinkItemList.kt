package com.example.coctailxmail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.coctailxmail.databinding.FragmentDrinkItemListBinding
import com.example.coctailxmail.viewmodel.DrinkMenuViewModel
import com.example.coctailxmail.views.DrinkItemsAdapter


class DrinkItemList : Fragment() {

    private val drinkMenuVM by activityViewModels<DrinkMenuViewModel>()
    private var _binding: FragmentDrinkItemListBinding? = null
    private val drinkItemsAdapter by lazy { DrinkItemsAdapter { drinkItem ->
        val bundle: Bundle = Bundle()
        bundle.putString("drinkIndex", drinkItem.idDrink.toString())
        findNavController().navigate(R.id.drinkDetailScreen, bundle)
    }
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val categoryIndex = arguments?.getString("categoryFilter")!!.toString()
        drinkMenuVM.getDrinkCategoryList(categoryIndex)
        return FragmentDrinkItemListBinding.inflate(inflater, container, false).apply {
            _binding = this
            drinkListView.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = drinkItemsAdapter
            }
        }.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        drinkMenuVM.state.observe(viewLifecycleOwner) { state ->
            drinkItemsAdapter.loadDrinkItems(state.drinkItemList)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}