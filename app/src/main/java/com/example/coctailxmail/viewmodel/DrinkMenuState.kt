package com.example.coctailxmail.viewmodel

import com.example.coctailxmail.model.entity.Category
import com.example.coctailxmail.model.entity.Drink
import com.example.coctailxmail.model.entity.DrinkItem

data class DrinkMenuState(val isLoading: Boolean = false, val drinkCategories: List<Category> = emptyList(),
val drinkItemList: List<DrinkItem> = emptyList(), val drinkList: List<Drink> = emptyList())
