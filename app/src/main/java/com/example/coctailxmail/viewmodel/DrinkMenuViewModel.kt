package com.example.coctailxmail.viewmodel

import androidx.compose.runtime.collectAsState
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.coctailxmail.model.entity.Category
import com.example.coctailxmail.model.entity.Drink
import com.example.coctailxmail.model.entity.DrinkItem
import com.example.coctailxmail.model.remote.CocktailRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

@HiltViewModel
class DrinkMenuViewModel @Inject constructor(private val repo: CocktailRepo): ViewModel() {

    private val _stateFlow = MutableStateFlow(DrinkMenuState())
    val stateFlow get() = _stateFlow.asStateFlow()

    private val _state = MutableLiveData(DrinkMenuState())
    val state: LiveData<DrinkMenuState> get() = _state


    init {
        getDrinkCategoriesList()
    }

    fun getDrinkCategoriesList(){
        _stateFlow.update{it.copy(isLoading = true)}
        viewModelScope.launch{
            val itemList = repo.getCategories()
            _state.isSuccess(itemList)
        }
        _stateFlow.update{it.copy(isLoading = true)}

    }

    fun getDrinkCategoryList(category: String){
        _stateFlow.update{it.copy(isLoading = true)}
        viewModelScope.launch{
            val itemList = repo.getDrinkListByCategory(category)
            _state.isSuccess(itemList)
            //_stateFlow.update{it.copy(drinkItemList = itemList)}
        }
        _stateFlow.update{it.copy(isLoading = false)}
    }

    fun getDrinkDetailsById(id_: String){
        _stateFlow.update{it.copy(isLoading = true)}
        viewModelScope.launch{
            val itemList = repo.getDrinkDetailsByIdNum(id_)
            _state.isSuccess(itemList)
            //_stateFlow.update{it.copy(drinkList = itemList)}
        }
        _stateFlow.update{it.copy(isLoading = false)}
    }

    private fun MutableLiveData<DrinkMenuState>.isLoading(isLoading: Boolean) {
        value = value?.copy(isLoading = isLoading)
    }

    private fun MutableLiveData<DrinkMenuState>.isSuccess(repoData: List<*>) {

        when(repoData[0]){
            is Category -> { value = value?.copy(isLoading = false, drinkCategories = repoData as List<Category>) }
            is DrinkItem -> { value = value?.copy(isLoading = false, drinkItemList = repoData as List<DrinkItem>) }
            is Drink -> { value = value?.copy(isLoading = false, drinkList = repoData as List<Drink>) }
        }
    }


}