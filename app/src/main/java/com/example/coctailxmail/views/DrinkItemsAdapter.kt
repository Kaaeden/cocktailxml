package com.example.coctailxmail.views

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.coctailxmail.databinding.CocktailCategoryItemBinding
import com.example.coctailxmail.databinding.DrinkItemCardBinding
import com.example.coctailxmail.model.entity.Category
import com.example.coctailxmail.model.entity.DrinkItem

class DrinkItemsAdapter(private val drinkItemClicked: (DrinkItem) -> Unit): RecyclerView.Adapter<DrinkItemsAdapter.DrinkItemsViewHolder>() {

private val drinkItems: MutableList<DrinkItem> = mutableListOf()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): DrinkItemsViewHolder = DrinkItemsViewHolder(parent).apply {

        itemView.setOnClickListener {
            val drinkItem = drinkItems[adapterPosition]
            drinkItemClicked(drinkItem)
        }
    }

    override fun onBindViewHolder(
        holder: DrinkItemsViewHolder,
        position: Int
    ) = holder.bindDrinkItems(drinkItems[position])

    override fun getItemCount(): Int = drinkItems.size

    fun loadDrinkItems(drinkItemList: List<DrinkItem>) {
        val startPosition = this.drinkItems.size
        this.drinkItems.addAll(drinkItemList)
        notifyItemRangeInserted(startPosition, drinkItemList.size)
    }

    class DrinkItemsViewHolder(
        private val binding: DrinkItemCardBinding
    ) : RecyclerView.ViewHolder(binding.root) {


        fun bindDrinkItems(drinkItem: DrinkItem) = with(binding) {
            drinkItemImage.load(drinkItem.strDrinkThumb)
            drinkItemName.text = drinkItem.strDrink

        }

        companion object {

            operator fun invoke(parent: ViewGroup): DrinkItemsViewHolder {
                val inflater = LayoutInflater.from(parent.context)
                val itemBinding = DrinkItemCardBinding.inflate(inflater, parent, false)
                return DrinkItemsViewHolder(itemBinding)
            }
        }

    }

}