package com.example.coctailxmail.views

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.coctailxmail.databinding.CocktailCategoryItemBinding
import com.example.coctailxmail.model.entity.Category

class DrinkMenuAdapter(private val categoryClicked: (Category) -> Unit) : RecyclerView.Adapter<DrinkMenuAdapter.DrinkMenuViewHolder>() {


    //Way to manage and update the Category list inside of this class.

    private val category: MutableList<Category> = mutableListOf()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): DrinkMenuViewHolder = DrinkMenuViewHolder(parent).apply {

        itemView.setOnClickListener {
            val category = category[adapterPosition]
            categoryClicked(category)
        }
    }

    override fun onBindViewHolder(
        holder: DrinkMenuViewHolder,
        position: Int
    ) = holder.bindCategories(category[position])

    override fun getItemCount(): Int = category.size

    fun loadCategories(category: List<Category>) {
        val startPosition = this.category.size
        this.category.addAll(category)
        notifyItemRangeInserted(startPosition, category.size)
    }

//Class that Connects XML Binding to Adapter

    class DrinkMenuViewHolder(
        private val binding: CocktailCategoryItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        //Uses With to reference XML [CocktailCategoryItem] Binding
        fun bindCategories(category: Category) = with(binding) {
            ctCategory.text = category.strCategory
        }

        //Companion Object - Pa
        companion object {

            operator fun invoke(parent: ViewGroup): DrinkMenuViewHolder {
                val inflater = LayoutInflater.from(parent.context)
                val itemBinding = CocktailCategoryItemBinding.inflate(inflater, parent, false)
                return DrinkMenuViewHolder(itemBinding)
            }
        }

    }

}