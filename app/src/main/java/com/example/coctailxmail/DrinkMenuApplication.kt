package com.example.coctailxmail

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class DrinkMenuApplication: Application() {
}